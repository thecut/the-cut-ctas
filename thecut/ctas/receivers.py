# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals


def add_call_to_actions_generic_relation(sender, **kwargs):
    from django.contrib.contenttypes.fields import GenericRelation
    from thecut.publishing.models import Content
    if issubclass(sender, Content):
        sender.add_to_class(
            'call_to_actions',
            GenericRelation('ctas.CallToAction',
                            content_type_field='source_content_type',
                            object_id_field='source_object_id')
        )
