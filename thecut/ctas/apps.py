# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import apps
from django.db.models import signals

from . import receivers


class AppConfig(apps.AppConfig):

    label = 'ctas'

    name = 'thecut.ctas'

    def ready(self):
        # Let's be "helpful", and add cta generic relation field to anything
        # that extends the Content model.
        signals.pre_init.connect(
            receivers.add_call_to_actions_generic_relation,
            dispatch_uid='thecut.ctas.add_call_to_actions_generic_relation')
