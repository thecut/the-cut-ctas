# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.contrib.contenttypes.models import ContentType


def get_call_to_actions_for_object(obj):
    """Returns active CallToActions for the object provided."""
    from thecut.ctas.models import CallToAction
    content_type = ContentType.objects.get_for_model(obj)
    call_to_actions = CallToAction.objects.active().filter(
        source_content_type=content_type, source_object_id=obj.id)
    return call_to_actions
