# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ctas', '0003_delete_ctas_with_no_source'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='calltoaction',
            options={'get_latest_by': 'publish_at', 'ordering': ['order', 'pk'], 'verbose_name': 'call-to-action'},
        ),
        migrations.AlterField(
            model_name='calltoaction',
            name='content_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='contenttypes.ContentType'),
        ),
        migrations.AlterField(
            model_name='calltoaction',
            name='source_content_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='+', to='contenttypes.ContentType'),
        ),
        migrations.AlterField(
            model_name='calltoaction',
            name='source_object_id',
            field=models.IntegerField(db_index=True),
        ),
    ]
