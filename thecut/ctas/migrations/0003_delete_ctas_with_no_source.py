# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django.db import migrations


def delete_ctas_with_no_source(apps, schema_editor):
    CallToAction = apps.get_model('ctas', 'CallToAction')
    CallToAction.objects.filter(source_object_id__isnull=True).delete()
    CallToAction.objects.filter(source_content_type__isnull=True).delete()


class Migration(migrations.Migration):

    dependencies = [
        ('ctas', '0002_auto_20170123_1413'),
    ]

    operations = [
        migrations.RunPython(delete_ctas_with_no_source,
                             migrations.RunPython.noop),
    ]
