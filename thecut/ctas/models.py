# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import fields, managers, receivers
from django.db import models
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.utils.encoding import python_2_unicode_compatible
from thecut.ordering.models import OrderMixin
from thecut.publishing.models import PublishableResource


@python_2_unicode_compatible
class CallToActionContentType(ContentType):

    objects = managers.CallToActionContentTypeManager()

    class Meta(object):
        proxy = True

    def __str__(self):
        return self.name.title()


@python_2_unicode_compatible
class AbstractCallToAction(OrderMixin, PublishableResource):
    """Call-to-action - links any object to any other object.

    """

    # Generic relation to the source object.
    source_content_type = models.ForeignKey(
        'contenttypes.ContentType', related_name='+', on_delete=models.CASCADE)
    source_object_id = models.IntegerField(db_index=True)
    source_content_object = GenericForeignKey('source_content_type',
                                              'source_object_id')

    # Generic relation to the target object.
    content_type = models.ForeignKey(
        'ctas.CallToActionContentType', related_name='+',
        on_delete=models.CASCADE)
    object_id = models.IntegerField(db_index=True)
    content_object = fields.CallToActionGenericForeignKey('content_type',
                                                          'object_id')

    title = models.CharField(max_length=200, blank=True, default='')

    image = models.ImageField(upload_to='uploads/ctas', blank=True, null=True)

    content = models.TextField(blank=True, default='')

    class Meta(OrderMixin.Meta, PublishableResource.Meta):
        abstract = True

    def __str__(self):
        return '{0}'.format(self.title or self.content_object)

    def get_absolute_url(self):
        if hasattr(self.content_object, 'get_absolute_url'):
            return self.content_object.get_absolute_url()
        return None

    def get_image(self):
        """Attempt to find an image to use."""
        if self.image:
            return self.image
        elif 'thecut.media' in settings.INSTALLED_APPS:
            obj = self.content_object
            if hasattr(obj, 'media') and hasattr(obj.media, 'get_image'):
                return obj.media.get_image()
        return None

    @property
    def name(self):
        return self.title


class CallToAction(AbstractCallToAction):

    class Meta(AbstractCallToAction.Meta):
        verbose_name = 'call-to-action'
