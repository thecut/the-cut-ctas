# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .api.urls import generate_urls as generate_api_urls
from .forms import CallToActionAdminForm
from .models import CallToAction
from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.contenttypes.admin import GenericStackedInline
from thecut.authorship.admin import AuthorshipMixin

try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse


class CallToActionAdmin(AuthorshipMixin, admin.ModelAdmin):

    fieldsets = [
        (None, {'fields': ['title', 'image', 'content']}),
        ('Publishing', {'fields': [('publish_at', 'is_enabled'), 'expire_at',
                                   'publish_by', 'is_featured',
                                   ('created_at', 'created_by'),
                                   ('updated_at', 'updated_by')],
                        'classes': ['collapse']}),
    ]

    list_display = ['__str__', 'publish_at', 'is_enabled', 'is_featured']

    list_filter = ['publish_at', 'is_enabled', 'is_featured']

    readonly_fields = ['created_at', 'created_by', 'updated_at', 'updated_by']


class CallToActionInline(GenericStackedInline):

    ct_field = 'source_content_type'

    ct_fk_field = 'source_object_id'

    form = CallToActionAdminForm

    model = CallToAction

    template = 'ctas/admin/ctas/_calltoaction_inline.html'


class CallToActionMixin(AuthorshipMixin, admin.ModelAdmin):

    inlines = [CallToActionInline]

    def _add_ctas_api_url_to_context(self, context):
        ctas_contenttypes_api_url = reverse(
            '{}:{}:contenttype_list'.format(
                self.admin_site.name, self._get_ctas_api_url_namespace()))
        context.update({
            'ctas_contenttypes_api_url': ctas_contenttypes_api_url})
        return context

    def _get_ctas_api_url_namespace(self):
        return 'ctas_api-{}-{}'.format(self.model._meta.app_label,
                                       self.model._meta.model_name)

    def add_view(self, *args, **kwargs):
        extra_context = kwargs.pop('extra_context', {})
        extra_context = self._add_ctas_api_url_to_context(extra_context)
        kwargs.update({'extra_context': extra_context})
        return super(CallToActionMixin, self).add_view(*args, **kwargs)

    def change_view(self, *args, **kwargs):
        extra_context = kwargs.pop('extra_context', {})
        extra_context = self._add_ctas_api_url_to_context(extra_context)
        kwargs.update({'extra_context': extra_context})
        return super(CallToActionMixin, self).change_view(*args, **kwargs)

    def get_urls(self, *args, **kwargs):
        ctas_api_urls = generate_api_urls(
            admin_site_name=self.admin_site.name,
            namespace=self._get_ctas_api_url_namespace())
        urlpatterns = [url(r'^ctas/api/', include(ctas_api_urls))]
        urlpatterns += super(CallToActionMixin, self).get_urls(*args, **kwargs)
        return urlpatterns
