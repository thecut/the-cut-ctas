# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from django import forms
from thecut.ctas.models import CallToAction


class CallToActionAdminForm(forms.ModelForm):

    class Meta(object):
        fields = ['content_type', 'object_id', 'title', 'image', 'content']
        model = CallToAction

    class Media(object):
        js = ['ctas/jquery.js', 'ctas/jquery.init.js', 'ctas/admin.js']

    def __init__(self, *args, **kwargs):
        super(CallToActionAdminForm, self).__init__(*args, **kwargs)
        self.fields['content_type'].label = 'Type'
        self.fields['object_id'].label = 'Destination'
        if 'content' in self.fields:
            self.fields['content'].label = 'Description'
