# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import serializers
from ..models import CallToActionContentType
from django.utils.decorators import method_decorator
from django.views.decorators.cache import never_cache
from rest_framework import authentication, generics, permissions, renderers
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView


class APIMixin(object):

    authentication_classes = [authentication.SessionAuthentication]

    permission_classes = [permissions.IsAdminUser]

    renderer_classes = [renderers.JSONRenderer]

    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(APIMixin, self).dispatch(*args, **kwargs)


class RootAPIView(APIMixin, APIView):

    def get(self, request, url_namespace, format=None):
        contenttype_list_url = reverse(
            '{0}:contenttype_list'.format(url_namespace), request=request)
        return Response(
            {'contentttypes': contenttype_list_url})


class ContentTypeListAPIView(APIMixin, generics.ListAPIView):

    queryset = CallToActionContentType.objects.all()

    serializer_class = serializers.ContentTypeSerializer


class ContentTypeRetrieveAPIView(APIMixin, generics.RetrieveAPIView):

    queryset = CallToActionContentType.objects.all()

    serializer_class = serializers.ContentTypeWithObjectsSerializer
