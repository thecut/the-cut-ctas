# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from .. import settings
from ..models import CallToActionContentType
from django.contrib.sites.shortcuts import get_current_site
from rest_framework import serializers
from rest_framework.reverse import reverse


class ContentTypeSerializer(serializers.HyperlinkedModelSerializer):

    id = serializers.ReadOnlyField(source='pk')

    url = serializers.SerializerMethodField()

    verbose_name = serializers.SerializerMethodField()

    verbose_name_plural = serializers.SerializerMethodField()

    class Meta(object):
        fields = ['url', 'id', 'verbose_name', 'verbose_name_plural']
        model = CallToActionContentType

    def get_url(self, content_type):
        namespace = self.context['view'].kwargs['url_namespace']
        return reverse('{0}:contenttype_detail'.format(namespace),
                       kwargs={'pk': content_type.pk},
                       request=self.context['request'],
                       format=self.context['format'])

    def get_verbose_name(self, content_type):
        return content_type.model_class()._meta.verbose_name.title()

    def get_verbose_name_plural(self, content_type):
        return content_type.model_class()._meta.verbose_name_plural.title()


class ContentTypeWithObjectsSerializer(ContentTypeSerializer):

    objects = serializers.SerializerMethodField()

    class Meta(ContentTypeSerializer.Meta):
        fields = ContentTypeSerializer.Meta.fields + ['objects']

    def get_objects(self, content_type):
        model = content_type.model_class()
        queryset = model.objects.all()
        if settings.SITE_FILTER and hasattr(model, 'site'):
            queryset = queryset.filter(
                site=get_current_site(self.context['request']))
        return GenericSerializer(queryset, many=True,
                                 model=content_type.model_class()).data


class GenericSerializer(serializers.ModelSerializer):

    id = serializers.ReadOnlyField(source='pk')

    name = serializers.SerializerMethodField()

    class Meta(object):
        fields = ['id', 'name']

    def __init__(self, queryset, model, **kwargs):
        self.Meta.model = model
        return super(GenericSerializer, self).__init__(queryset, **kwargs)

    def get_name(self, instance):
        return instance.__str__().title()
