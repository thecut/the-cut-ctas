# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
from . import views
from django.conf.urls import include, url
from rest_framework.urlpatterns import format_suffix_patterns


def generate_urls(admin_site_name, namespace):

    full_namespace = '{}:{}'.format(admin_site_name, namespace)

    urls = [

        url(r'^$', views.RootAPIView.as_view(),
            {'url_namespace': full_namespace}, name='root'),

        url(r'^contenttypes/$',
            views.ContentTypeListAPIView.as_view(),
            {'url_namespace': full_namespace}, name='contenttype_list'),

        url(r'^contenttypes/(?P<pk>\d+)/$',
            views.ContentTypeRetrieveAPIView.as_view(),
            {'url_namespace': full_namespace}, name='contenttype_detail'),

    ]

    urlpatterns = [url(r'^', include(urls, namespace=namespace))]

    urlpatterns = format_suffix_patterns(urlpatterns, allowed=['json'])

    return urlpatterns
