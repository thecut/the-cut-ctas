ctas.jQuery(document).ready(function($) {

    var ctasContentTypesApiUrl = $('link[itemprop="ctas_contenttypes"][href]').attr('href');

    function updateObjectSelect($objectSelect, contentTypePk, objectSelectValue) {
        $objectSelect.empty().append('<option value="">---------</option>').val('');
        if (contentTypePk) {
            $.ajax({
                type: 'GET',
                url: ctasContentTypesApiUrl + contentTypePk + '/',
                success: function(data) {
                    $.each(data.objects, function(index, item) {
                        var option = $('<option />').attr('value', item.id).html(item.name);
                        $objectSelect.append(option);
                    });
                    if (objectSelectValue) {
                        $objectSelect.val(objectSelectValue);
                    }
                }
            });
        }
    }

    var $inlineGroup = $('#ctas-calltoaction-source_content_type-source_object_id-group.inline-group');
    $inlineGroup.addClass('js-enabled');

    // Replace object select when the content type is changed.
    $inlineGroup.on('change', '.form-row select[name$="-content_type"]', function(event) {
        var $objectSelect = $(this).closest('fieldset').find('.form-row select[name$="-object_id"]');
        var contentTypePk = $(this).val();
        updateObjectSelect($objectSelect, contentTypePk);
    });

    // Update label when object is changed.
    $inlineGroup.on('change', '.form-row select', function(event) {
        var $inlineRelated = $(this).closest('.inline-related');
        var label = $inlineRelated.find('.inline_label');
        var objectText = $inlineRelated.find('.form-row select[name$="-object_id"] option:selected').text();
        label.text(objectText);
    });

    // Replace object id input with object select.
    $inlineGroup.find('.form-row input[name$="-object_id"]').each(function() {
        var $input = $(this);
        var $objectSelect = $('<select id="' + $input.attr('id') + '" name="' + $input.attr('name') + '"></select>');
        var contentTypePk = $input.closest('.inline-related').find('.form-row select[name$="-content_type"]').val();
        var inputValue = $input.val();
        $input.replaceWith($objectSelect);
        updateObjectSelect($objectSelect, contentTypePk, inputValue);
    });

});
